#!/bin/bash
# variables
git_repo="/root/energy-consumption-of-gpu-benchmarks/"
local_git_repo="/home/mjay/GPU_benchmark_energy/"
result_repo="$git_repo/results/test_04_11_22/"
installation_script="$git_repo/code/install_scripts/installation.sh"
env_script="$git_repo/code/install_scripts/get_env_ready.sh"
start_script="$git_repo/code/start_exp.py"
es_folder="/root/energy_scope/"

# installation
ssh root@chifflet-2 "mkdir $git_repo"
ssh root@chifflet-2 "mkdir $git_repo/results/"
ssh root@chifflet-2 "mkdir $result_repo"
rsync -avzh $local_git_repo/code/ root@chifflet-2:$git_repo/code/
rsync -avzh $local_git_repo/NAS_benchmark_binaries/ root@chifflet-2:$git_repo/NAS_benchmark_binaries/
rsync -avzh $local_git_repo/software-installation/ root@chifflet-2:$git_repo/software-installation/
ssh root@chifflet-2 "bash $installation_script"
ssh root@chifflet-2 "bash $env_script"

# compile benchmarks
rsync -avzh NPB-GPU/ root@chifflet-2:NPB-GPU/ 
ssh root@chifflet-2 "bash $git_repo/NAS_benchmark_binaries/compile_all.sh"
: '
# start experiments
ssh root@chifflet-2 "bash $git_repo/code/install_scripts/launch_all_frontale.sh"
scp -r root@chifflet-2:$git_repo/results/ $local_git_repo
'

# More scp to make sure everything is saved in case of failure
# Lots of loops to adapt to past failed experiments

for j in {1..1}
do
    for tool in CarbonTrackerTool
    do
        ssh root@chifflet-2 "python3.7 $start_script --git_repo $git_repo --result_folder $result_repo --energy_scope_folder $es_folder --$tool --repetitions 1"
        scp -r root@chifflet-2:$git_repo/results/ $local_git_repo
    done
done
: '
# only once is needed for codecarbon
ssh root@chifflet-2 "python3.7 $start_script --git_repo $git_repo --result_folder $result_repo --energy_scope_folder $es_folder --CodeCarbon --repetitions 1"
scp -r root@chifflet-2:$git_repo/results/ $local_git_repo

for j in {1..4}
do
    for tool in EnergyScope
    do
        ssh root@chifflet-2 "python3.7 $start_script --git_repo $git_repo --result_folder $result_repo --energy_scope_folder $es_folder --$tool --repetitions 1"
        scp -r root@chifflet-2:$git_repo/results/ $local_git_repo
    done
done

for j in {1..3}
do
    for i in {0..3} # 0 à 3
    do
         # ExperimentImpactTracker
        for tool in ExperimentImpactTracker
        do
            ssh root@chifflet-2 "python3.7 $start_script --git_repo $git_repo --result_folder $result_repo --energy_scope_folder $es_folder --$tool --repetitions 1 --benchmark_id $i"
            scp -r root@chifflet-2:$git_repo/results/ $local_git_repo
        done
    done
done

for j in {1..8}
do
    for tool in CodeCarbon CarbonTrackerTool
    do
        ssh root@chifflet-2 "python3.7 $start_script --git_repo $git_repo --result_folder $result_repo --energy_scope_folder $es_folder --monitor_one_process --$tool --repetitions 1"
        scp -r root@chifflet-2:$git_repo/results/ $local_git_repo
    done
done
'