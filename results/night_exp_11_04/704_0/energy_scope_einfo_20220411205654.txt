{
  "date": "2022/04/11 20:58:34",
  "from_appli": {
    "jobid": "20220411205654",
    "user": "",
    "command": "/root/energy-consumption-of-gpu-benchmarks/results/night_exp_11_04/704_0//gpu0/scripts/script_final.sh",
    "comment": "",
    "tags": {
      "es_appli_total": {
        "start": "2022/04/11 20:56:57.000000",
        "stop": "2022/04/11 20:58:18.000000"
      }
    }
  },
  "arch": {
    "data": {
      "joule(J)": {
        "etotal(J)": 43790,
        "ecpu(J)": 41548,
        "edram(J)": 2242,
        "etotal(%)": 100.0
      }
    },
    "arch": {
      "gemini-1": {
        "data": {
          "joule(J)": {
            "etotal(J)": 43790,
            "ecpu(J)": 41548,
            "edram(J)": 2242,
            "etotal(%)": 100.0
          }
        },
        "arch": {
          "0": {
            "data": {
              "joule(J)": {
                "etotal(J)": 7286,
                "ecpu(J)": 6188,
                "edram(J)": 1098,
                "etotal(%)": 16.64
              }
            }
          },
          "1": {
            "data": {
              "joule(J)": {
                "etotal(J)": 7001,
                "ecpu(J)": 5857,
                "edram(J)": 1144,
                "etotal(%)": 15.99
              }
            }
          },
          "gpu-nvidia-0": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3701,
                "ecpu(J)": 3701,
                "edram(J)": 0,
                "etotal(%)": 8.45
              }
            }
          },
          "gpu-nvidia-1": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3699,
                "ecpu(J)": 3699,
                "edram(J)": 0,
                "etotal(%)": 8.45
              }
            }
          },
          "gpu-nvidia-2": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3620,
                "ecpu(J)": 3620,
                "edram(J)": 0,
                "etotal(%)": 8.27
              }
            }
          },
          "gpu-nvidia-3": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3659,
                "ecpu(J)": 3659,
                "edram(J)": 0,
                "etotal(%)": 8.36
              }
            }
          },
          "gpu-nvidia-4": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3625,
                "ecpu(J)": 3625,
                "edram(J)": 0,
                "etotal(%)": 8.28
              }
            }
          },
          "gpu-nvidia-5": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3697,
                "ecpu(J)": 3697,
                "edram(J)": 0,
                "etotal(%)": 8.44
              }
            }
          },
          "gpu-nvidia-6": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3738,
                "ecpu(J)": 3738,
                "edram(J)": 0,
                "etotal(%)": 8.54
              }
            }
          },
          "gpu-nvidia-7": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3764,
                "ecpu(J)": 3764,
                "edram(J)": 0,
                "etotal(%)": 8.6
              }
            }
          }
        }
      }
    }
  }
}