{
  "date": "2022/04/11 20:07:23",
  "from_appli": {
    "jobid": "20220411200542",
    "user": "",
    "command": "/root/energy-consumption-of-gpu-benchmarks/results/night_exp_11_04/431_0//gpu0/scripts/script_final.sh",
    "comment": "",
    "tags": {
      "es_appli_total": {
        "start": "2022/04/11 20:05:45.000000",
        "stop": "2022/04/11 20:07:06.000000"
      }
    }
  },
  "arch": {
    "data": {
      "joule(J)": {
        "etotal(J)": 43596,
        "ecpu(J)": 41364,
        "edram(J)": 2232,
        "etotal(%)": 100.0
      }
    },
    "arch": {
      "gemini-1": {
        "data": {
          "joule(J)": {
            "etotal(J)": 43596,
            "ecpu(J)": 41364,
            "edram(J)": 2232,
            "etotal(%)": 100.0
          }
        },
        "arch": {
          "0": {
            "data": {
              "joule(J)": {
                "etotal(J)": 7271,
                "ecpu(J)": 6174,
                "edram(J)": 1097,
                "etotal(%)": 16.68
              }
            }
          },
          "1": {
            "data": {
              "joule(J)": {
                "etotal(J)": 6945,
                "ecpu(J)": 5810,
                "edram(J)": 1135,
                "etotal(%)": 15.93
              }
            }
          },
          "gpu-nvidia-0": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3683,
                "ecpu(J)": 3683,
                "edram(J)": 0,
                "etotal(%)": 8.45
              }
            }
          },
          "gpu-nvidia-1": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3680,
                "ecpu(J)": 3680,
                "edram(J)": 0,
                "etotal(%)": 8.44
              }
            }
          },
          "gpu-nvidia-2": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3603,
                "ecpu(J)": 3603,
                "edram(J)": 0,
                "etotal(%)": 8.26
              }
            }
          },
          "gpu-nvidia-3": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3641,
                "ecpu(J)": 3641,
                "edram(J)": 0,
                "etotal(%)": 8.35
              }
            }
          },
          "gpu-nvidia-4": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3621,
                "ecpu(J)": 3621,
                "edram(J)": 0,
                "etotal(%)": 8.31
              }
            }
          },
          "gpu-nvidia-5": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3678,
                "ecpu(J)": 3678,
                "edram(J)": 0,
                "etotal(%)": 8.44
              }
            }
          },
          "gpu-nvidia-6": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3720,
                "ecpu(J)": 3720,
                "edram(J)": 0,
                "etotal(%)": 8.53
              }
            }
          },
          "gpu-nvidia-7": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3754,
                "ecpu(J)": 3754,
                "edram(J)": 0,
                "etotal(%)": 8.61
              }
            }
          }
        }
      }
    }
  }
}