for minute in {0..950..30}
do
    echo $minute
    scp -r root@gemini-1:/root/energy-consumption-of-gpu-benchmarks/results/ /home/mjay/GPU_benchmark_energy/ | at now + $minute minute
done