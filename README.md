# An experimental comparison of software-based power meters: GPU side
Author: Mathilde JAY   
Last update: July, the 19. 2022

This repository was created with the objective to compare tool measuring the energy consumption of other software - or software-based power meters as we like to call them. This project focuses on software monitoring GPUs energy consumption.

## Directory layout
    .
    ├── code                    # Experiment implementation files
    ├── graphes                 # Saved graphs
    ├── CPU_benchmark_binaries  # CPU Benchmark binaries
    ├── GPU_benchmark_binaries  # GPU Benchmark binaries
    ├── results                 # Experiment outputs
    ├── requirements.txt        # Software requirements
    ├── LICENSE.md
    └── README.md

More details on the code directory:

    .
    ├── ...
    ├── code        
    │   ├── install_scripts           # Image installation scripts
    │   ├── templates                 # Script templates
    │   └── start_exp.py              # Start experiments           
    │   ├── utils                     # Classes and methods useful for both experiments and processing
    │   │   └── experiments.py              # Methods to create experiments and retrieve its metadata
    │   │   └── tools.py                    # Methods to start the tools
    │   │   └── process_results.py          # Merge all experiments into a dataset easy to process
    │   │   └── carbontracker_parser.py     # Rewriting Carbon Tracker parser to deal with error
    │   │   └── plots.py                    # Functions to plot insideful graphs 
    │   ├── analysis                  # Result analysis notebooks
    │   │   └── bar energy plot.ipynb       # Bar plots to compare all tool total energy results
    │   │   └── MAPE_MRSE.ipynb             # Exploring different evaluation metrics
    │   │   └── compute_total_energy_study.ipynb # Computing the energy consumed by all experiments
    │   │   └── offset.ipynb                # Studying offset between Energy Scope and the wattmeters
    │   │   └── online tools.ipynb          # Adding results from online tools
    │   │   └── overhead.ipynb              # Studying the overhead in energy of the tools
    │   │   └── timeseries.ipynb            # Plots of timeseries
    └── ...

## Software wattmeters selected
- Energy Scope
- Code Carbon
- Experiment Impact Tracker
- Carbon Tracker
- Green Algorithm
- ML CO2 Impact

## Reservations g5K
Here are example on how to deploy and reserve grid5000 nodes:

```
user@flyon:~$ oarsub -t deploy -t exotic -p "host='gemini-1.lyon.grid5000.fr'" -l walltime=2 -I
user@flyon:~$ oarsub -t deploy -r "2022-04-20 19:00:00" -t exotic -p "host='gemini-1.lyon.grid5000.fr'" -l walltime=14:00
user@flyon:~$ oarsub -C $JOB_ID
```

## Deploy the image
To reduce the effect of the os, we need to use the mose simple os we need.
To do that we will use the kadeploy option of grid5000.
```
user@flyon:~$ kadeploy3 -k -e ubuntu2004-x64-min -f $OAR_FILE_NODES
user@flyon:~$ ssh -A root@gemini-1.lyon.grid5000.fr
```
or, if the ressource was reserved:
```
user@flyon:~$ oarsub -C JOBID
user@flyon:~$ kadeploy3 -k -e ubuntu2004-x64-min -m gemini-1
user@flyon:~$ ssh -A root@gemini-1.lyon.grid5000.fr
```
Then install only what is needed:
```
user@gemini-1:~$ bash energy-consumption-of-gpu-benchmarks/code/install_scripts/installation.sh
```

## Execution of the experiments

Before starting the experiment, let's make sure that the environnement is reproducible by fixing a set of variables:

(All implemented in the script: `get_env_ready.sh`)   
- Activate the MSR module: `sudo modprobe msr`   
- Disable hyperthreading: `echo off > /sys/devices/system/cpu/smt/control`   
- Disable Turbo Boost: `sudo wrmsr -a 0x1a0 0x4000850089`   
- Disable C-states: `sudo cpupower idle-set -D 0`   
- Get the maximum frequency of the CPU: `cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq`   
- Change CPU frequency governor: `sudo cpupower frequency-set -g performance`   
- Set min and max frequency: `sudo cpupower frequency-set -d MIN_FREQ && sudo cpupower frequency-set -u MAX_FREQ `   
MIN_FREQ = MAX_FREQ   
- Use `i7z` command to verify that frequency is maximum and that Turboboost and Hyperthreading are disabled. 
     
To make sure that the benchmarks will compile, check the compute capability in make.def. It needs to match your GPU.   
For gemini, change it to `COMPUTE_CAPABILITY = -gencode arch=compute_70,code=sm_70`.   

So to start the experiments, execute the following code:
```
user@gemini-1:~$ bash /root/energy-consumption-of-gpu-benchmarks/code/install_scripts/get_env_ready.sh
user@gemini-1:~$ bash /root/energy-consumption-of-gpu-benchmarks/code/install_scripts/launch_all.sh
```
The issue with the approach is that it is hard to retrieve the results.
I would advise to do everything from the frontal, as it is implemented in `code/install_scripts/launch_frontal_gemini-1.sh`

## Saving the image
### To save the image:
```bash
user@flyon:~$ tgz-g5k -m gemini-1 -f ~/public/ubuntu2004-min-image_22_03_22.tgz
kaenv3 -p ubuntu2004-x64-min -u deploy > image_22_03_22.yaml
```

It will be accessible at http://public.lille.grid5000.fr/~mjay/ubuntu2004-min-image_21_03_22.tgz
with yaml file:
```
name: ubuntu2004-x64-min-image_21_03_22
version: 2022022514
arch: x86_64
description: ubuntu 20.04 (focal) for x64 - min
author: support-staff@lists.grid5000.fr
visibility: private
destructive: false
os: linux
image:
  file: http://public.lille.grid5000.fr/~mjay/ubuntu2004-min-image_21_03_22.tgz
  # or ubuntu2004-min-image_21_03_22.tgz is the yaml file is in the same folder
  kind: tar
  compression: gzip
postinstalls:
- archive: server:///grid5000/postinstalls/g5k-postinstall.tgz
  compression: gzip
  script: g5k-postinstall --net netplan --disk-aliases
boot:
  kernel: "/boot/vmlinuz"
  initrd: "/boot/initrd.img"
  kernel_params: ''
filesystem: ext4
partition_type: 131
multipart: false
```
You need to change the url, the name, public to private, compression to gzip.

### Once the image is saved
```
user@flyon:~$ oarsub -I -t deploy (...)
# only if reserved in advance
user@flyon:~$ oarsub -C JOBID
user@flyon:~$ kadeploy3 -k -a image_22_03_22.yaml -f $OAR_NODEFILE
user@flyon:~$ tmux
tmux:~$ ssh -A root@gemini-1.lyon.grid5000.fr 
```

## If you need to install the tools locally
```
module load miniconda3
conda create --name gpu_benchmark python=3.7
conda activate gpu_benchmark
conda install -c conda-forge scikit-learn
conda install -c conda-forge pandas
conda install -c conda-forge matplotlib
conda install -c conda-forge py-cpuinfo
conda install -c conda-forge psutil
conda install -c anaconda requests

conda install -c codecarbon -c conda-forge codecarbon
pip install git+https://github.com/Breakend/experiment-impact-tracker.git
pip install carbontracker
pip install pyJoules
```
If you want to use the notebook:
```
conda install ipykernel
python -m ipykernel install --user --name gpu_benchmark --display-name "python 3.7 (GPU Benchmark)"
```

## Analysis 
The script `process_results.py` can be used to process the results, the output being a single dataframe containing all results.   
A separate dataframe will contain the timeseries of the power used by Energy Scope and the Wattmeters.   
`wattmeter.ipynb` can be used to retrieve the wattmeter data separately.   
`process_results.py` was not made to be used directly, you will need to modify the script with you variables.   
    
The analysis notebooks can be found in `code/analysis`.
    
We focused on:
- Offset between the wattmeters and the tools (`offset.ipynb`)
- Timseries comparisons (`timeseries.ipynb`)
- Comparison in energy (`bar energy plot.ipynb`)
- Other evaluation metrics (`MAPE_MRSE.ipynb`)
- Overhead in energy of the softwares (`overhead.ipynb`)
- Computation of energy through online tools (`online tools.ipynb`)


## How was decided which benchmark application to use
### NAS Parallel benchmarks for CPUs

### NAS Parallel benchmarks for GPUs 
The benchmarks we used are the NAS Parallel Benchmarks implemented in CUDA for GPUs:
- Github repository: https://github.com/GMAP/NPB-GPU/tree/master/CUDA 

Let's study which application to use.     
Please provide both the binary and the gpu.config to the script. Make sure the config file wasn't modified.

Notes:
- IS class D doesn't work
- ft class D & E don't work
- sp class D & E don't work
- cg & mg require higher DRAM power than other benchmarks: the data load takes most of the duration

| Application | Class | Duration on Chifflet (seconds) |
|--|--|--|
|ft|C|11|
| |D|0|
| |E|0|
|sp|C|15|
| |D|1|
| |E|0|
|mg|C|12|
| |D|88|
|bt|C|30|
| |D|36|
|cg|C|17|
| |D|1135|
|lu|C|34|
| |D|510|
|is|D|1|
| |E|0|
|ep|D|30|
| |E|490|

(For GEMINI `COMPUTE_CAPABILITY = -gencode arch=compute_70,code=sm_70`)
| Application | Class | Duration on gemini-1 (seconds) |
|--|--|--|
|mg|C|<1|
|  |D|15|
|lu|C|9|
| |D|200|
|ep|D|4.77|
| |E|67|
|cg|C|1.47|
| |D|141.96| 

Let's choose all benchmark that take more than 15 seconds to run, to make sure all tools will work.
Perhaps we can remove cg D because it's too long.
To simplify, let's select:

| Application | Class |
|--|--|
|mg|D|
|lu|D|
|ep|E|

### Idle
A file identical to the benchmark so they can be used similarly.
```
params = {
    'tuple': [('idle', 'sh')],
    'repetition': ['', ''],
    }
```
It only contains a sleep command.

### Stress
EP is puting the CPU/GPU at its maximal utilization.

## Issues
Below is the list of the issues I got with the tools and how I solved them.

### EIT
python 3.9.2
```
AttributeError: 'xml.etree.ElementTree.Element' object has no attribute 'getiterator'
```
python 3.7
```
    multiprocessing.set_start_method("fork")
  File "...lib/python3.7/multiprocessing/context.py", line 242, in set_start_method
    raise RuntimeError('context has already been set')
RuntimeError: context has already been set
```
Solution: I created a script that launch scripts one by one.
It still doesn't work with only one process.

If the utilisation is close to 0:
`ZeroDivisionError: float division by zero`

### PyJoules
It doesn't seem to work with multiprocessessing, so it was dropped.
`ValueError: ctypes objects containing pointers cannot be pickled`

### CarbonTracker
The parser file needed to be modified.

### Deploying machines
Sometimes the terminal would freeze, as if the image was killed.
I can create another one without have to make a new reservation.
- oarstat -u shows that the job is still in mode ready
- ping doesn't find the node

So we concluded that the issue came from the image. The fact that it was saved brings too much entropy so it would crash often.
I created a new image from scratch and I didn't get any issues so it seems like it's a good explanation.


# License

This work is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

