#!/bin/bash
start_script="/root/energy-consumption-of-gpu-benchmarks/code/start_exp.py"
git_repo="/root/energy-consumption-of-gpu-benchmarks/"
result_repo="/root/energy-consumption-of-gpu-benchmarks/results/night_exp_11_04/"
es_folder="/root/energy_scope/"
for i in {0..7}
do
   for j in {1..10}
   do
      for tool in ExperimentImpactTracker CarbonTrackerTool CodeCarbon PyJoules EnergyScope
      do
         python3.7 $start_script --git_repo $git_repo --result_folder $result_repo --energy_scope_folder $es_folder --$tool --repetitions 1 --benchmark_id $i
         python3.7 $start_script --git_repo $git_repo --result_folder $result_repo --energy_scope_folder $es_folder --monitor_one_process --$tool --repetitions 1 --benchmark_id $i
      done
   done
done