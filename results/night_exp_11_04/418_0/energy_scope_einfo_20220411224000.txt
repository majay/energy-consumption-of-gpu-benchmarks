{
  "date": "2022/04/11 22:43:22",
  "from_appli": {
    "jobid": "20220411224000",
    "user": "",
    "command": "/root/energy-consumption-of-gpu-benchmarks/results/night_exp_11_04/418_0//gpu0/scripts/script_final.sh",
    "comment": "",
    "tags": {
      "es_appli_total": {
        "start": "2022/04/11 22:40:04.000000",
        "stop": "2022/04/11 22:43:04.000000"
      }
    }
  },
  "arch": {
    "data": {
      "joule(J)": {
        "etotal(J)": 95622,
        "ecpu(J)": 91043,
        "edram(J)": 4579,
        "etotal(%)": 100.0
      }
    },
    "arch": {
      "gemini-1": {
        "data": {
          "joule(J)": {
            "etotal(J)": 95622,
            "ecpu(J)": 91043,
            "edram(J)": 4579,
            "etotal(%)": 100.0
          }
        },
        "arch": {
          "0": {
            "data": {
              "joule(J)": {
                "etotal(J)": 15662,
                "ecpu(J)": 13430,
                "edram(J)": 2232,
                "etotal(%)": 16.38
              }
            }
          },
          "1": {
            "data": {
              "joule(J)": {
                "etotal(J)": 15079,
                "ecpu(J)": 12732,
                "edram(J)": 2347,
                "etotal(%)": 15.77
              }
            }
          },
          "gpu-nvidia-0": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8014,
                "ecpu(J)": 8014,
                "edram(J)": 0,
                "etotal(%)": 8.38
              }
            }
          },
          "gpu-nvidia-1": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8188,
                "ecpu(J)": 8188,
                "edram(J)": 0,
                "etotal(%)": 8.56
              }
            }
          },
          "gpu-nvidia-2": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8014,
                "ecpu(J)": 8014,
                "edram(J)": 0,
                "etotal(%)": 8.38
              }
            }
          },
          "gpu-nvidia-3": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8100,
                "ecpu(J)": 8100,
                "edram(J)": 0,
                "etotal(%)": 8.47
              }
            }
          },
          "gpu-nvidia-4": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8085,
                "ecpu(J)": 8085,
                "edram(J)": 0,
                "etotal(%)": 8.46
              }
            }
          },
          "gpu-nvidia-5": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8185,
                "ecpu(J)": 8185,
                "edram(J)": 0,
                "etotal(%)": 8.56
              }
            }
          },
          "gpu-nvidia-6": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8277,
                "ecpu(J)": 8277,
                "edram(J)": 0,
                "etotal(%)": 8.66
              }
            }
          },
          "gpu-nvidia-7": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8018,
                "ecpu(J)": 8018,
                "edram(J)": 0,
                "etotal(%)": 8.39
              }
            }
          }
        }
      }
    }
  }
}