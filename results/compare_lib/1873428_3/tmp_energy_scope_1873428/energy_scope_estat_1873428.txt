{
  "date": "2022/03/01 10:00:44",
  "pgm": "energy_scope@inria",
  "duration(sec)": 44.799,
  "from_appli": {
    "jobid": "1873428",
    "user": "",
    "command": "",
    "comment": ""
  },
  "arch": {
    "data": {
      "total": {
        "node": 1,
        "phy": 2,
        "core": 28,
        "gpu": 2
      },
      "node_list": [
        "chifflet-8"
      ],
      "joule(J)": {
        "etotal(J)": 12889,
        "ecpu(J)": 9542,
        "edram(J)": 3347,
        "etotal(%)": 100.0
      },
      "power(W)": {
        "min": 0.0,
        "max": 0.0,
        "mean": 0.0
      }
    },
    "arch": {
      "chifflet-8": {
        "data": {
          "acquisition(ms)": {
            "min": 0.0,
            "max": 0.0,
            "mean": 0.0
          }
        },
        "arch": {
          "0": {
            "arch": {
              "0": {
                "arch": {},
                "data": {}
              },
              "2": {
                "arch": {},
                "data": {}
              },
              "4": {
                "arch": {},
                "data": {}
              },
              "6": {
                "arch": {},
                "data": {}
              },
              "8": {
                "arch": {},
                "data": {}
              },
              "10": {
                "arch": {},
                "data": {}
              },
              "12": {
                "arch": {},
                "data": {}
              },
              "14": {
                "arch": {},
                "data": {}
              },
              "16": {
                "arch": {},
                "data": {}
              },
              "18": {
                "arch": {},
                "data": {}
              },
              "20": {
                "arch": {},
                "data": {}
              },
              "22": {
                "arch": {},
                "data": {}
              },
              "24": {
                "arch": {},
                "data": {}
              },
              "26": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Intel(R)Xeon(R)CPUE5-2680v4@2.40GHz",
              "core": 14,
              "joule(J)": {
                "etotal(J)": 2651,
                "ecpu(J)": 979,
                "edram(J)": 1672,
                "etotal(%)": 20.57
              }
            }
          },
          "1": {
            "arch": {
              "1": {
                "arch": {},
                "data": {}
              },
              "3": {
                "arch": {},
                "data": {}
              },
              "5": {
                "arch": {},
                "data": {}
              },
              "7": {
                "arch": {},
                "data": {}
              },
              "9": {
                "arch": {},
                "data": {}
              },
              "11": {
                "arch": {},
                "data": {}
              },
              "13": {
                "arch": {},
                "data": {}
              },
              "15": {
                "arch": {},
                "data": {}
              },
              "17": {
                "arch": {},
                "data": {}
              },
              "19": {
                "arch": {},
                "data": {}
              },
              "21": {
                "arch": {},
                "data": {}
              },
              "23": {
                "arch": {},
                "data": {}
              },
              "25": {
                "arch": {},
                "data": {}
              },
              "27": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Intel(R)Xeon(R)CPUE5-2680v4@2.40GHz",
              "core": 14,
              "joule(J)": {
                "etotal(J)": 3071,
                "ecpu(J)": 1396,
                "edram(J)": 1675,
                "etotal(%)": 23.83
              }
            }
          },
          "gpu-nvidia-0": {
            "arch": {
              "1000": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "GeForce GTX 1080 Ti",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 6770,
                "ecpu(J)": 6770,
                "edram(J)": 0,
                "etotal(%)": 52.53
              }
            }
          },
          "gpu-nvidia-1": {
            "arch": {
              "1001": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "GeForce GTX 1080 Ti",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 397,
                "ecpu(J)": 397,
                "edram(J)": 0,
                "etotal(%)": 3.08
              }
            }
          }
        }
      }
    }
  }
}