# An experimental comparison of software-based power meters: GPU side
Author: Mathilde JAY   
Last update: July, the 19. 2022

- [About the project](#about-the-project)
- [How to use this repository](#how-to-use-this-repository)
    - [Building the environement](#building-the-environement)
    - [Start experiments](#start-experiments)
    - [Analyse results](#analyse-results)
- [Contact](#contact)


## About the project
### NAS parallel benchmarks
### Selected software-based power meters

## How to use this repository
### Building the environement


### Start experiments
This repository contains scripts automating the deployement of series of experiments

### Analyse results
## Contact
