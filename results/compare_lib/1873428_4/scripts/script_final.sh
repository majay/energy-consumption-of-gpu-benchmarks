#!/bin/bash
benchmark_binary_path="/home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu1/ep.D"
chmod 777 $benchmark_binary_path
sleep_before=1
sleep_after=1
sleep $sleep_before
echo "BENCHMARK_TAG start_benchmark $(date '+%Y/%m/%dT%H:%M:%S.%6N')"
$benchmark_binary_path
echo "BENCHMARK_TAG stop_benchmark $(date '+%Y/%m/%dT%H:%M:%S.%6N')"
sleep $sleep_after
