{
  "date": "2022/04/13 18:49:50",
  "pgm": "energy_scope@inria",
  "duration(sec)": 718,
  "from_appli": {
    "jobid": "20220413203724",
    "user": "",
    "command": "/root/energy-consumption-of-gpu-benchmarks//results/night_exp_13_04_1/128_0//gpu0/scripts/script_final.sh",
    "comment": ""
  },
  "arch": {
    "data": {
      "total": {
        "node": 1,
        "phy": 2,
        "core": 40,
        "gpu": 8
      },
      "node_list": [
        "gemini-1"
      ],
      "joule(J)": {
        "etotal(J)": 393449,
        "ecpu(J)": 373511,
        "edram(J)": 19938,
        "etotal(%)": 100.0
      },
      "energy_est": {
        "etotal(J)": 723729,
        "etotal(kWh)": 0.201,
        "model": "joule_model_202203"
      },
      "carbon": {
        "country": "FR",
        "carbon(gCO2)": 10.271,
        "model": "carbon_model_202203"
      }
    },
    "arch": {
      "gemini-1": {
        "data": {
          "acquisition(ms)": {
            "min": 515.0,
            "max": 859.0,
            "mean": 520.812
          },
          "power(W)": {
            "min": 474.57,
            "max": 795.262,
            "mean": 547.42
          }
        },
        "arch": {
          "0": {
            "arch": {
              "0": {
                "arch": {},
                "data": {}
              },
              "1": {
                "arch": {},
                "data": {}
              },
              "2": {
                "arch": {},
                "data": {}
              },
              "3": {
                "arch": {},
                "data": {}
              },
              "4": {
                "arch": {},
                "data": {}
              },
              "5": {
                "arch": {},
                "data": {}
              },
              "6": {
                "arch": {},
                "data": {}
              },
              "7": {
                "arch": {},
                "data": {}
              },
              "8": {
                "arch": {},
                "data": {}
              },
              "9": {
                "arch": {},
                "data": {}
              },
              "10": {
                "arch": {},
                "data": {}
              },
              "11": {
                "arch": {},
                "data": {}
              },
              "12": {
                "arch": {},
                "data": {}
              },
              "13": {
                "arch": {},
                "data": {}
              },
              "14": {
                "arch": {},
                "data": {}
              },
              "15": {
                "arch": {},
                "data": {}
              },
              "16": {
                "arch": {},
                "data": {}
              },
              "17": {
                "arch": {},
                "data": {}
              },
              "18": {
                "arch": {},
                "data": {}
              },
              "19": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Intel(R)Xeon(R)CPUE5-2698v4@2.20GHz",
              "core": 20,
              "joule(J)": {
                "etotal(J)": 64012,
                "ecpu(J)": 54273,
                "edram(J)": 9739,
                "etotal(%)": 16.27
              }
            }
          },
          "1": {
            "arch": {
              "20": {
                "arch": {},
                "data": {}
              },
              "21": {
                "arch": {},
                "data": {}
              },
              "22": {
                "arch": {},
                "data": {}
              },
              "23": {
                "arch": {},
                "data": {}
              },
              "24": {
                "arch": {},
                "data": {}
              },
              "25": {
                "arch": {},
                "data": {}
              },
              "26": {
                "arch": {},
                "data": {}
              },
              "27": {
                "arch": {},
                "data": {}
              },
              "28": {
                "arch": {},
                "data": {}
              },
              "29": {
                "arch": {},
                "data": {}
              },
              "30": {
                "arch": {},
                "data": {}
              },
              "31": {
                "arch": {},
                "data": {}
              },
              "32": {
                "arch": {},
                "data": {}
              },
              "33": {
                "arch": {},
                "data": {}
              },
              "34": {
                "arch": {},
                "data": {}
              },
              "35": {
                "arch": {},
                "data": {}
              },
              "36": {
                "arch": {},
                "data": {}
              },
              "37": {
                "arch": {},
                "data": {}
              },
              "38": {
                "arch": {},
                "data": {}
              },
              "39": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Intel(R)Xeon(R)CPUE5-2698v4@2.20GHz",
              "core": 20,
              "joule(J)": {
                "etotal(J)": 61838,
                "ecpu(J)": 51639,
                "edram(J)": 10199,
                "etotal(%)": 15.72
              }
            }
          },
          "gpu-nvidia-0": {
            "arch": {
              "1000": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 32760,
                "ecpu(J)": 32760,
                "edram(J)": 0,
                "etotal(%)": 8.33
              }
            }
          },
          "gpu-nvidia-1": {
            "arch": {
              "1001": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 33537,
                "ecpu(J)": 33537,
                "edram(J)": 0,
                "etotal(%)": 8.52
              }
            }
          },
          "gpu-nvidia-2": {
            "arch": {
              "1002": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 33565,
                "ecpu(J)": 33565,
                "edram(J)": 0,
                "etotal(%)": 8.53
              }
            }
          },
          "gpu-nvidia-3": {
            "arch": {
              "1003": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 34134,
                "ecpu(J)": 34134,
                "edram(J)": 0,
                "etotal(%)": 8.68
              }
            }
          },
          "gpu-nvidia-4": {
            "arch": {
              "1004": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 33134,
                "ecpu(J)": 33134,
                "edram(J)": 0,
                "etotal(%)": 8.42
              }
            }
          },
          "gpu-nvidia-5": {
            "arch": {
              "1005": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 33494,
                "ecpu(J)": 33494,
                "edram(J)": 0,
                "etotal(%)": 8.51
              }
            }
          },
          "gpu-nvidia-6": {
            "arch": {
              "1006": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 33994,
                "ecpu(J)": 33994,
                "edram(J)": 0,
                "etotal(%)": 8.64
              }
            }
          },
          "gpu-nvidia-7": {
            "arch": {
              "1007": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 32981,
                "ecpu(J)": 32981,
                "edram(J)": 0,
                "etotal(%)": 8.38
              }
            }
          }
        }
      }
    }
  }
}