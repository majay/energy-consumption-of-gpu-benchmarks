{
  "date": "2022/04/11 20:05:18",
  "from_appli": {
    "jobid": "20220411200338",
    "user": "",
    "command": "/root/energy-consumption-of-gpu-benchmarks/results/night_exp_11_04/525_0//gpu0/scripts/script_final.sh",
    "comment": "",
    "tags": {
      "es_appli_total": {
        "start": "2022/04/11 20:03:41.000000",
        "stop": "2022/04/11 20:05:02.000000"
      }
    }
  },
  "arch": {
    "data": {
      "joule(J)": {
        "etotal(J)": 43610,
        "ecpu(J)": 41378,
        "edram(J)": 2232,
        "etotal(%)": 100.0
      }
    },
    "arch": {
      "gemini-1": {
        "data": {
          "joule(J)": {
            "etotal(J)": 43610,
            "ecpu(J)": 41378,
            "edram(J)": 2232,
            "etotal(%)": 100.0
          }
        },
        "arch": {
          "0": {
            "data": {
              "joule(J)": {
                "etotal(J)": 7265,
                "ecpu(J)": 6170,
                "edram(J)": 1095,
                "etotal(%)": 16.66
              }
            }
          },
          "1": {
            "data": {
              "joule(J)": {
                "etotal(J)": 6946,
                "ecpu(J)": 5809,
                "edram(J)": 1137,
                "etotal(%)": 15.93
              }
            }
          },
          "gpu-nvidia-0": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3682,
                "ecpu(J)": 3682,
                "edram(J)": 0,
                "etotal(%)": 8.44
              }
            }
          },
          "gpu-nvidia-1": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3680,
                "ecpu(J)": 3680,
                "edram(J)": 0,
                "etotal(%)": 8.44
              }
            }
          },
          "gpu-nvidia-2": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3602,
                "ecpu(J)": 3602,
                "edram(J)": 0,
                "etotal(%)": 8.26
              }
            }
          },
          "gpu-nvidia-3": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3640,
                "ecpu(J)": 3640,
                "edram(J)": 0,
                "etotal(%)": 8.35
              }
            }
          },
          "gpu-nvidia-4": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3624,
                "ecpu(J)": 3624,
                "edram(J)": 0,
                "etotal(%)": 8.31
              }
            }
          },
          "gpu-nvidia-5": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3678,
                "ecpu(J)": 3678,
                "edram(J)": 0,
                "etotal(%)": 8.43
              }
            }
          },
          "gpu-nvidia-6": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3719,
                "ecpu(J)": 3719,
                "edram(J)": 0,
                "etotal(%)": 8.53
              }
            }
          },
          "gpu-nvidia-7": {
            "data": {
              "joule(J)": {
                "etotal(J)": 3774,
                "ecpu(J)": 3774,
                "edram(J)": 0,
                "etotal(%)": 8.65
              }
            }
          }
        }
      }
    }
  }
}