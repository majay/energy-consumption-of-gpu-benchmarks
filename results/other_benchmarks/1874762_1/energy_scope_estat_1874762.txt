{
  "date": "2022/03/10 17:08:30",
  "pgm": "energy_scope@inria",
  "duration(sec)": 294.77,
  "from_appli": {
    "jobid": "1874762",
    "user": "",
    "command": "",
    "comment": ""
  },
  "arch": {
    "data": {
      "total": {
        "node": 1,
        "phy": 2,
        "core": 28,
        "gpu": 2
      },
      "node_list": [
        "chifflet-6"
      ],
      "joule(J)": {
        "etotal(J)": 48404,
        "ecpu(J)": 32065,
        "edram(J)": 16339,
        "etotal(%)": 100.0
      },
      "power(W)": {
        "min": 0.0,
        "max": 0.0,
        "mean": 0.0
      }
    },
    "arch": {
      "chifflet-6": {
        "data": {
          "acquisition(ms)": {
            "min": 0.0,
            "max": 0.0,
            "mean": 0.0
          }
        },
        "arch": {
          "0": {
            "arch": {
              "0": {
                "arch": {},
                "data": {}
              },
              "2": {
                "arch": {},
                "data": {}
              },
              "4": {
                "arch": {},
                "data": {}
              },
              "6": {
                "arch": {},
                "data": {}
              },
              "8": {
                "arch": {},
                "data": {}
              },
              "10": {
                "arch": {},
                "data": {}
              },
              "12": {
                "arch": {},
                "data": {}
              },
              "14": {
                "arch": {},
                "data": {}
              },
              "16": {
                "arch": {},
                "data": {}
              },
              "18": {
                "arch": {},
                "data": {}
              },
              "20": {
                "arch": {},
                "data": {}
              },
              "22": {
                "arch": {},
                "data": {}
              },
              "24": {
                "arch": {},
                "data": {}
              },
              "26": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Intel(R)Xeon(R)CPUE5-2680v4@2.40GHz",
              "core": 14,
              "joule(J)": {
                "etotal(J)": 15846,
                "ecpu(J)": 7528,
                "edram(J)": 8318,
                "etotal(%)": 32.74
              }
            }
          },
          "1": {
            "arch": {
              "1": {
                "arch": {},
                "data": {}
              },
              "3": {
                "arch": {},
                "data": {}
              },
              "5": {
                "arch": {},
                "data": {}
              },
              "7": {
                "arch": {},
                "data": {}
              },
              "9": {
                "arch": {},
                "data": {}
              },
              "11": {
                "arch": {},
                "data": {}
              },
              "13": {
                "arch": {},
                "data": {}
              },
              "15": {
                "arch": {},
                "data": {}
              },
              "17": {
                "arch": {},
                "data": {}
              },
              "19": {
                "arch": {},
                "data": {}
              },
              "21": {
                "arch": {},
                "data": {}
              },
              "23": {
                "arch": {},
                "data": {}
              },
              "25": {
                "arch": {},
                "data": {}
              },
              "27": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Intel(R)Xeon(R)CPUE5-2680v4@2.40GHz",
              "core": 14,
              "joule(J)": {
                "etotal(J)": 13381,
                "ecpu(J)": 5360,
                "edram(J)": 8021,
                "etotal(%)": 27.64
              }
            }
          },
          "gpu-nvidia-0": {
            "arch": {
              "1000": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "GeForce GTX 1080 Ti",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 16626,
                "ecpu(J)": 16626,
                "edram(J)": 0,
                "etotal(%)": 34.35
              }
            }
          },
          "gpu-nvidia-1": {
            "arch": {
              "1001": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "GeForce GTX 1080 Ti",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 2551,
                "ecpu(J)": 2551,
                "edram(J)": 0,
                "etotal(%)": 5.27
              }
            }
          }
        }
      }
    }
  }
}