#!/bin/bash
# variables
local_git_path="/root/energy-consumption-of-gpu-benchmarks/"
frontale_git_path="/home/mjay/GPU_benchmark_energy/"
result_path="$local_git_path/results/night_exp_08_11/"
installation_script="$local_git_path/code/install_scripts/installation.sh"
env_script="$local_git_path/code/install_scripts/get_env_ready.sh"
start_script="$local_git_path/code/start_exp.py"
es_folder="/root/energy_scope/"
benchmark_binary_dir="/CPU_benchmark_binaries/"

# installation

ssh root@gemini-1 "mkdir $local_git_path"
ssh root@gemini-1 "mkdir $local_git_path/results/"
ssh root@gemini-1 "mkdir $result_path"
rsync -avzh $frontale_git_path/code/ root@gemini-1:$local_git_path/code/
#rsync -avzh $frontale_git_path/$benchmark_binary_dir/ root@gemini-1:$local_git_path/$benchmark_binary_dir/
#rsync -avzh $frontale_git_path/software-installation/ root@gemini-1:$local_git_path/software-installation/
#ssh root@gemini-1 "bash $installation_script"
#ssh root@gemini-1 "bash $env_script"

# compile benchmarks
#rsync -avzh NPB-GPU/ root@gemini-1:NPB-GPU/ 
#ssh root@gemini-1 "bash $local_git_path/$benchmark_binary_dir/compile_all.sh"

# start experiments
#ssh root@gemini-1 "bash $local_git_path/code/install_scripts/launch_all_frontale.sh"
#scp -r root@gemini-1:$local_git_path/results/ $frontale_git_path

# More scp to make sure everything is saved in case of failure
for j in {1..10} # 1 to 10
do
    for tool in CarbonTrackerTool CodeCarbon EnergyScope
    do
        ssh root@gemini-1 "python3.7 $start_script --git_repo $local_git_path --result_folder $result_path --energy_scope_folder $es_folder --$tool --repetitions 1 --benchmark_binary_dir $local_git_path/$benchmark_binary_dir/ --CPU"
        scp -r root@gemini-1:$local_git_path/results/ $frontale_git_path
    done
done

for j in {1..10} # 1 to 10
do
    for i in {0..3} # 0 to 3
    do
        ssh root@gemini-1 "python3.7 $start_script --git_repo $local_git_path --result_folder $result_path --energy_scope_folder $es_folder --ExperimentImpactTracker --repetitions 1 --benchmark_id $i --benchmark_binary_dir $local_git_path/$benchmark_binary_dir/ --CPU"
        scp -r root@gemini-1:$local_git_path/results/ $frontale_git_path
    done
done
