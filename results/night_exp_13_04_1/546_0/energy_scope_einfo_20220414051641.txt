{
  "date": "2022/04/14 05:29:01",
  "from_appli": {
    "jobid": "20220414051641",
    "user": "",
    "command": "/root/energy-consumption-of-gpu-benchmarks//results/night_exp_13_04_1/546_0//gpu0/scripts/script_final.sh",
    "comment": "",
    "tags": {
      "es_appli_total": {
        "start": "2022/04/14 05:16:51.000000",
        "stop": "2022/04/14 05:28:45.000000"
      }
    }
  },
  "arch": {
    "data": {
      "joule(J)": {
        "etotal(J)": 390490,
        "ecpu(J)": 370795,
        "edram(J)": 19695,
        "etotal(%)": 100.0
      }
    },
    "arch": {
      "gemini-1": {
        "data": {
          "joule(J)": {
            "etotal(J)": 390490,
            "ecpu(J)": 370795,
            "edram(J)": 19695,
            "etotal(%)": 100.0
          }
        },
        "arch": {
          "0": {
            "data": {
              "joule(J)": {
                "etotal(J)": 63793,
                "ecpu(J)": 54150,
                "edram(J)": 9643,
                "etotal(%)": 16.34
              }
            }
          },
          "1": {
            "data": {
              "joule(J)": {
                "etotal(J)": 61093,
                "ecpu(J)": 51041,
                "edram(J)": 10052,
                "etotal(%)": 15.65
              }
            }
          },
          "gpu-nvidia-0": {
            "data": {
              "joule(J)": {
                "etotal(J)": 33367,
                "ecpu(J)": 33367,
                "edram(J)": 0,
                "etotal(%)": 8.54
              }
            }
          },
          "gpu-nvidia-1": {
            "data": {
              "joule(J)": {
                "etotal(J)": 33192,
                "ecpu(J)": 33192,
                "edram(J)": 0,
                "etotal(%)": 8.5
              }
            }
          },
          "gpu-nvidia-2": {
            "data": {
              "joule(J)": {
                "etotal(J)": 32175,
                "ecpu(J)": 32175,
                "edram(J)": 0,
                "etotal(%)": 8.24
              }
            }
          },
          "gpu-nvidia-3": {
            "data": {
              "joule(J)": {
                "etotal(J)": 32853,
                "ecpu(J)": 32853,
                "edram(J)": 0,
                "etotal(%)": 8.41
              }
            }
          },
          "gpu-nvidia-4": {
            "data": {
              "joule(J)": {
                "etotal(J)": 33653,
                "ecpu(J)": 33653,
                "edram(J)": 0,
                "etotal(%)": 8.62
              }
            }
          },
          "gpu-nvidia-5": {
            "data": {
              "joule(J)": {
                "etotal(J)": 34097,
                "ecpu(J)": 34097,
                "edram(J)": 0,
                "etotal(%)": 8.73
              }
            }
          },
          "gpu-nvidia-6": {
            "data": {
              "joule(J)": {
                "etotal(J)": 33629,
                "ecpu(J)": 33629,
                "edram(J)": 0,
                "etotal(%)": 8.61
              }
            }
          },
          "gpu-nvidia-7": {
            "data": {
              "joule(J)": {
                "etotal(J)": 32638,
                "ecpu(J)": 32638,
                "edram(J)": 0,
                "etotal(%)": 8.36
              }
            }
          }
        }
      }
    }
  }
}