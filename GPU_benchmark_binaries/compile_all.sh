#!/bin/bash
cd /home/mjay/NPB-GPU/CUDA/
for bench in cg
do
    for class in C D E
    do
        for gpu_id in {0..7}
        do
            python3 /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/modify_config.py --gpu_id $gpu_id --nas_config_repo /home/mjay/NPB-GPU/CUDA/config/gpu.config # modify config file
            echo "config file: "
            head -1 /home/mjay/NPB-GPU/CUDA/config/gpu.config
            make clean
            make $bench CLASS=$class
            mkdir /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu$gpu_id
            cp /home/mjay/NPB-GPU/CUDA/bin/$bench.$class /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu$gpu_id/
        done
    done
done