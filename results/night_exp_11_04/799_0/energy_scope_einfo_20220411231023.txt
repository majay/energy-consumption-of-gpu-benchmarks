{
  "date": "2022/04/11 23:13:45",
  "from_appli": {
    "jobid": "20220411231023",
    "user": "",
    "command": "/root/energy-consumption-of-gpu-benchmarks/results/night_exp_11_04/799_0//gpu0/scripts/script_final.sh",
    "comment": "",
    "tags": {
      "es_appli_total": {
        "start": "2022/04/11 23:10:27.000000",
        "stop": "2022/04/11 23:13:27.000000"
      }
    }
  },
  "arch": {
    "data": {
      "joule(J)": {
        "etotal(J)": 95660,
        "ecpu(J)": 91069,
        "edram(J)": 4591,
        "etotal(%)": 100.0
      }
    },
    "arch": {
      "gemini-1": {
        "data": {
          "joule(J)": {
            "etotal(J)": 95660,
            "ecpu(J)": 91069,
            "edram(J)": 4591,
            "etotal(%)": 100.0
          }
        },
        "arch": {
          "0": {
            "data": {
              "joule(J)": {
                "etotal(J)": 15697,
                "ecpu(J)": 13456,
                "edram(J)": 2241,
                "etotal(%)": 16.41
              }
            }
          },
          "1": {
            "data": {
              "joule(J)": {
                "etotal(J)": 15123,
                "ecpu(J)": 12773,
                "edram(J)": 2350,
                "etotal(%)": 15.81
              }
            }
          },
          "gpu-nvidia-0": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8012,
                "ecpu(J)": 8012,
                "edram(J)": 0,
                "etotal(%)": 8.38
              }
            }
          },
          "gpu-nvidia-1": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8186,
                "ecpu(J)": 8186,
                "edram(J)": 0,
                "etotal(%)": 8.56
              }
            }
          },
          "gpu-nvidia-2": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8013,
                "ecpu(J)": 8013,
                "edram(J)": 0,
                "etotal(%)": 8.38
              }
            }
          },
          "gpu-nvidia-3": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8098,
                "ecpu(J)": 8098,
                "edram(J)": 0,
                "etotal(%)": 8.47
              }
            }
          },
          "gpu-nvidia-4": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8056,
                "ecpu(J)": 8056,
                "edram(J)": 0,
                "etotal(%)": 8.42
              }
            }
          },
          "gpu-nvidia-5": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8183,
                "ecpu(J)": 8183,
                "edram(J)": 0,
                "etotal(%)": 8.55
              }
            }
          },
          "gpu-nvidia-6": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8275,
                "ecpu(J)": 8275,
                "edram(J)": 0,
                "etotal(%)": 8.65
              }
            }
          },
          "gpu-nvidia-7": {
            "data": {
              "joule(J)": {
                "etotal(J)": 8017,
                "ecpu(J)": 8017,
                "edram(J)": 0,
                "etotal(%)": 8.38
              }
            }
          }
        }
      }
    }
  }
}