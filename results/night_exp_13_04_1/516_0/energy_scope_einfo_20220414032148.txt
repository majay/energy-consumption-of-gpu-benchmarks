{
  "date": "2022/04/14 03:34:08",
  "from_appli": {
    "jobid": "20220414032148",
    "user": "",
    "command": "/root/energy-consumption-of-gpu-benchmarks//results/night_exp_13_04_1/516_0//gpu0/scripts/script_final.sh",
    "comment": "",
    "tags": {
      "es_appli_total": {
        "start": "2022/04/14 03:21:58.000000",
        "stop": "2022/04/14 03:33:54.000000"
      }
    }
  },
  "arch": {
    "data": {
      "joule(J)": {
        "etotal(J)": 391862,
        "ecpu(J)": 372094,
        "edram(J)": 19768,
        "etotal(%)": 100.0
      }
    },
    "arch": {
      "gemini-1": {
        "data": {
          "joule(J)": {
            "etotal(J)": 391862,
            "ecpu(J)": 372094,
            "edram(J)": 19768,
            "etotal(%)": 100.0
          }
        },
        "arch": {
          "0": {
            "data": {
              "joule(J)": {
                "etotal(J)": 63485,
                "ecpu(J)": 53797,
                "edram(J)": 9688,
                "etotal(%)": 16.2
              }
            }
          },
          "1": {
            "data": {
              "joule(J)": {
                "etotal(J)": 61146,
                "ecpu(J)": 51066,
                "edram(J)": 10080,
                "etotal(%)": 15.6
              }
            }
          },
          "gpu-nvidia-0": {
            "data": {
              "joule(J)": {
                "etotal(J)": 32469,
                "ecpu(J)": 32469,
                "edram(J)": 0,
                "etotal(%)": 8.29
              }
            }
          },
          "gpu-nvidia-1": {
            "data": {
              "joule(J)": {
                "etotal(J)": 34288,
                "ecpu(J)": 34288,
                "edram(J)": 0,
                "etotal(%)": 8.75
              }
            }
          },
          "gpu-nvidia-2": {
            "data": {
              "joule(J)": {
                "etotal(J)": 32272,
                "ecpu(J)": 32272,
                "edram(J)": 0,
                "etotal(%)": 8.24
              }
            }
          },
          "gpu-nvidia-3": {
            "data": {
              "joule(J)": {
                "etotal(J)": 33978,
                "ecpu(J)": 33978,
                "edram(J)": 0,
                "etotal(%)": 8.67
              }
            }
          },
          "gpu-nvidia-4": {
            "data": {
              "joule(J)": {
                "etotal(J)": 32659,
                "ecpu(J)": 32659,
                "edram(J)": 0,
                "etotal(%)": 8.33
              }
            }
          },
          "gpu-nvidia-5": {
            "data": {
              "joule(J)": {
                "etotal(J)": 33284,
                "ecpu(J)": 33284,
                "edram(J)": 0,
                "etotal(%)": 8.49
              }
            }
          },
          "gpu-nvidia-6": {
            "data": {
              "joule(J)": {
                "etotal(J)": 34641,
                "ecpu(J)": 34641,
                "edram(J)": 0,
                "etotal(%)": 8.84
              }
            }
          },
          "gpu-nvidia-7": {
            "data": {
              "joule(J)": {
                "etotal(J)": 33640,
                "ecpu(J)": 33640,
                "edram(J)": 0,
                "etotal(%)": 8.58
              }
            }
          }
        }
      }
    }
  }
}