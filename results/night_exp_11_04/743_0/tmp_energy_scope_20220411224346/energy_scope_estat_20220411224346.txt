{
  "date": "2022/04/11 20:47:12",
  "pgm": "energy_scope@inria",
  "duration(sec)": 194,
  "from_appli": {
    "jobid": "20220411224346",
    "user": "",
    "command": "/root/energy-consumption-of-gpu-benchmarks/results/night_exp_11_04/743_0//gpu0/scripts/script_final.sh",
    "comment": ""
  },
  "arch": {
    "data": {
      "total": {
        "node": 1,
        "phy": 2,
        "core": 40,
        "gpu": 8
      },
      "node_list": [
        "gemini-1"
      ],
      "joule(J)": {
        "etotal(J)": 103440,
        "ecpu(J)": 98487,
        "edram(J)": 4953,
        "etotal(%)": 100.0
      },
      "energy_est": {
        "etotal(J)": 192680,
        "etotal(kWh)": 0.054,
        "model": "joule_model_202203"
      },
      "carbon": {
        "country": "FR",
        "carbon(gCO2)": 2.759,
        "model": "carbon_model_202203"
      }
    },
    "arch": {
      "gemini-1": {
        "data": {
          "acquisition(ms)": {
            "min": 515.0,
            "max": 521.0,
            "mean": 517.136
          },
          "power(W)": {
            "min": 530.432,
            "max": 534.527,
            "mean": 532.018
          }
        },
        "arch": {
          "0": {
            "arch": {
              "0": {
                "arch": {},
                "data": {}
              },
              "1": {
                "arch": {},
                "data": {}
              },
              "2": {
                "arch": {},
                "data": {}
              },
              "3": {
                "arch": {},
                "data": {}
              },
              "4": {
                "arch": {},
                "data": {}
              },
              "5": {
                "arch": {},
                "data": {}
              },
              "6": {
                "arch": {},
                "data": {}
              },
              "7": {
                "arch": {},
                "data": {}
              },
              "8": {
                "arch": {},
                "data": {}
              },
              "9": {
                "arch": {},
                "data": {}
              },
              "10": {
                "arch": {},
                "data": {}
              },
              "11": {
                "arch": {},
                "data": {}
              },
              "12": {
                "arch": {},
                "data": {}
              },
              "13": {
                "arch": {},
                "data": {}
              },
              "14": {
                "arch": {},
                "data": {}
              },
              "15": {
                "arch": {},
                "data": {}
              },
              "16": {
                "arch": {},
                "data": {}
              },
              "17": {
                "arch": {},
                "data": {}
              },
              "18": {
                "arch": {},
                "data": {}
              },
              "19": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Intel(R)Xeon(R)CPUE5-2698v4@2.20GHz",
              "core": 20,
              "joule(J)": {
                "etotal(J)": 16953,
                "ecpu(J)": 14539,
                "edram(J)": 2414,
                "etotal(%)": 16.39
              }
            }
          },
          "1": {
            "arch": {
              "20": {
                "arch": {},
                "data": {}
              },
              "21": {
                "arch": {},
                "data": {}
              },
              "22": {
                "arch": {},
                "data": {}
              },
              "23": {
                "arch": {},
                "data": {}
              },
              "24": {
                "arch": {},
                "data": {}
              },
              "25": {
                "arch": {},
                "data": {}
              },
              "26": {
                "arch": {},
                "data": {}
              },
              "27": {
                "arch": {},
                "data": {}
              },
              "28": {
                "arch": {},
                "data": {}
              },
              "29": {
                "arch": {},
                "data": {}
              },
              "30": {
                "arch": {},
                "data": {}
              },
              "31": {
                "arch": {},
                "data": {}
              },
              "32": {
                "arch": {},
                "data": {}
              },
              "33": {
                "arch": {},
                "data": {}
              },
              "34": {
                "arch": {},
                "data": {}
              },
              "35": {
                "arch": {},
                "data": {}
              },
              "36": {
                "arch": {},
                "data": {}
              },
              "37": {
                "arch": {},
                "data": {}
              },
              "38": {
                "arch": {},
                "data": {}
              },
              "39": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Intel(R)Xeon(R)CPUE5-2698v4@2.20GHz",
              "core": 20,
              "joule(J)": {
                "etotal(J)": 16360,
                "ecpu(J)": 13821,
                "edram(J)": 2539,
                "etotal(%)": 15.82
              }
            }
          },
          "gpu-nvidia-0": {
            "arch": {
              "1000": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8664,
                "ecpu(J)": 8664,
                "edram(J)": 0,
                "etotal(%)": 8.38
              }
            }
          },
          "gpu-nvidia-1": {
            "arch": {
              "1001": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8852,
                "ecpu(J)": 8852,
                "edram(J)": 0,
                "etotal(%)": 8.56
              }
            }
          },
          "gpu-nvidia-2": {
            "arch": {
              "1002": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8665,
                "ecpu(J)": 8665,
                "edram(J)": 0,
                "etotal(%)": 8.38
              }
            }
          },
          "gpu-nvidia-3": {
            "arch": {
              "1003": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8757,
                "ecpu(J)": 8757,
                "edram(J)": 0,
                "etotal(%)": 8.47
              }
            }
          },
          "gpu-nvidia-4": {
            "arch": {
              "1004": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8723,
                "ecpu(J)": 8723,
                "edram(J)": 0,
                "etotal(%)": 8.43
              }
            }
          },
          "gpu-nvidia-5": {
            "arch": {
              "1005": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8848,
                "ecpu(J)": 8848,
                "edram(J)": 0,
                "etotal(%)": 8.55
              }
            }
          },
          "gpu-nvidia-6": {
            "arch": {
              "1006": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8948,
                "ecpu(J)": 8948,
                "edram(J)": 0,
                "etotal(%)": 8.65
              }
            }
          },
          "gpu-nvidia-7": {
            "arch": {
              "1007": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8670,
                "ecpu(J)": 8670,
                "edram(J)": 0,
                "etotal(%)": 8.38
              }
            }
          }
        }
      }
    }
  }
}