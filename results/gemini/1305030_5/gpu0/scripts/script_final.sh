chmod 777 /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu0/mg.D
chmod 777 /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu1/mg.D
chmod 777 /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu2/mg.D
chmod 777 /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu3/mg.D
chmod 777 /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu4/mg.D
chmod 777 /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu5/mg.D
chmod 777 /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu6/mg.D
chmod 777 /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu7/mg.D
#!/bin/bash
benchmark_binary_path="/home/mjay/GPU_benchmark_energy/code/launch_parallel.sh /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu0/mg.D /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu1/mg.D /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu2/mg.D /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu3/mg.D /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu4/mg.D /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu5/mg.D /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu6/mg.D /home/mjay/GPU_benchmark_energy/NAS_benchmark_binaries/gpu7/mg.D"
chmod 777 $benchmark_binary_path
sleep_before=1
sleep_after=1
gpu_number=0
sleep $sleep_before
echo "BENCHMARK_TAG start_benchmark GPU $gpu_number DATE $(date '+%Y/%m/%dT%H:%M:%S.%6N')"
$benchmark_binary_path
echo "BENCHMARK_TAG stop_benchmark GPU $gpu_number DATE $(date '+%Y/%m/%dT%H:%M:%S.%6N')"
sleep $sleep_after
