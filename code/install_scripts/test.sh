#!/bin/bash
bash /root/energy-consumption-of-gpu-benchmarks/code/install_scripts/get_env_ready.sh
python3.7 /root/energy-consumption-of-gpu-benchmarks/code/start_exp.py --git_repo /root/energy-consumption-of-gpu-benchmarks/ --result_folder /root/energy-consumption-of-gpu-benchmarks/results/chifflet-tests/ --energy_scope_folder /root/energy_scope/ --EnergyScope --repetitions 1 --benchmark_id 1