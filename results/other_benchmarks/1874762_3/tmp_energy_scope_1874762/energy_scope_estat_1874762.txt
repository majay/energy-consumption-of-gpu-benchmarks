{
  "date": "2022/03/10 16:39:25",
  "pgm": "energy_scope@inria",
  "duration(sec)": 1476.329,
  "from_appli": {
    "jobid": "1874762",
    "user": "",
    "command": "",
    "comment": ""
  },
  "arch": {
    "data": {
      "total": {
        "node": 1,
        "phy": 2,
        "core": 28,
        "gpu": 2
      },
      "node_list": [
        "chifflet-6"
      ],
      "joule(J)": {
        "etotal(J)": 352134,
        "ecpu(J)": 219801,
        "edram(J)": 132333,
        "etotal(%)": 100.0
      },
      "energy_est": {
        "etotal(J)": 588346.64,
        "etotal(kWh)": 0.163,
        "model": "joule_model_202203"
      },
      "carbon": {
        "country": "FR",
        "carbon(gCO2)": 8.329,
        "model": "carbon_model_202203"
      },
      "power(W)": {
        "min": 0.0,
        "max": 0.0,
        "mean": 0.0
      }
    },
    "arch": {
      "chifflet-6": {
        "data": {
          "acquisition(ms)": {
            "min": 510.0,
            "max": 586.0,
            "mean": 518.556
          },
          "power(W)": {
            "min": 71.828,
            "max": 348.369,
            "mean": 238.642
          }
        },
        "arch": {
          "0": {
            "arch": {
              "0": {
                "arch": {},
                "data": {}
              },
              "2": {
                "arch": {},
                "data": {}
              },
              "4": {
                "arch": {},
                "data": {}
              },
              "6": {
                "arch": {},
                "data": {}
              },
              "8": {
                "arch": {},
                "data": {}
              },
              "10": {
                "arch": {},
                "data": {}
              },
              "12": {
                "arch": {},
                "data": {}
              },
              "14": {
                "arch": {},
                "data": {}
              },
              "16": {
                "arch": {},
                "data": {}
              },
              "18": {
                "arch": {},
                "data": {}
              },
              "20": {
                "arch": {},
                "data": {}
              },
              "22": {
                "arch": {},
                "data": {}
              },
              "24": {
                "arch": {},
                "data": {}
              },
              "26": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Intel(R)Xeon(R)CPUE5-2680v4@2.40GHz",
              "core": 14,
              "joule(J)": {
                "etotal(J)": 128351,
                "ecpu(J)": 60200,
                "edram(J)": 68151,
                "etotal(%)": 36.45
              }
            }
          },
          "1": {
            "arch": {
              "1": {
                "arch": {},
                "data": {}
              },
              "3": {
                "arch": {},
                "data": {}
              },
              "5": {
                "arch": {},
                "data": {}
              },
              "7": {
                "arch": {},
                "data": {}
              },
              "9": {
                "arch": {},
                "data": {}
              },
              "11": {
                "arch": {},
                "data": {}
              },
              "13": {
                "arch": {},
                "data": {}
              },
              "15": {
                "arch": {},
                "data": {}
              },
              "17": {
                "arch": {},
                "data": {}
              },
              "19": {
                "arch": {},
                "data": {}
              },
              "21": {
                "arch": {},
                "data": {}
              },
              "23": {
                "arch": {},
                "data": {}
              },
              "25": {
                "arch": {},
                "data": {}
              },
              "27": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Intel(R)Xeon(R)CPUE5-2680v4@2.40GHz",
              "core": 14,
              "joule(J)": {
                "etotal(J)": 96614,
                "ecpu(J)": 32432,
                "edram(J)": 64182,
                "etotal(%)": 27.44
              }
            }
          },
          "gpu-nvidia-0": {
            "arch": {
              "1000": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "GeForce GTX 1080 Ti",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 114287,
                "ecpu(J)": 114287,
                "edram(J)": 0,
                "etotal(%)": 32.46
              }
            }
          },
          "gpu-nvidia-1": {
            "arch": {
              "1001": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "GeForce GTX 1080 Ti",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 12882,
                "ecpu(J)": 12882,
                "edram(J)": 0,
                "etotal(%)": 3.66
              }
            }
          }
        }
      }
    }
  }
}