{
  "date": "2022/04/11 21:10:04",
  "pgm": "energy_scope@inria",
  "duration(sec)": 194,
  "from_appli": {
    "jobid": "20220411230638",
    "user": "",
    "command": "/root/energy-consumption-of-gpu-benchmarks/results/night_exp_11_04/383_0//gpu0/scripts/script_final.sh",
    "comment": ""
  },
  "arch": {
    "data": {
      "total": {
        "node": 1,
        "phy": 2,
        "core": 40,
        "gpu": 8
      },
      "node_list": [
        "gemini-1"
      ],
      "joule(J)": {
        "etotal(J)": 103380,
        "ecpu(J)": 98418,
        "edram(J)": 4962,
        "etotal(%)": 100.0
      },
      "energy_est": {
        "etotal(J)": 192620,
        "etotal(kWh)": 0.054,
        "model": "joule_model_202203"
      },
      "carbon": {
        "country": "FR",
        "carbon(gCO2)": 2.759,
        "model": "carbon_model_202203"
      }
    },
    "arch": {
      "gemini-1": {
        "data": {
          "acquisition(ms)": {
            "min": 515.0,
            "max": 520.0,
            "mean": 516.832
          },
          "power(W)": {
            "min": 528.431,
            "max": 534.577,
            "mean": 532.019
          }
        },
        "arch": {
          "0": {
            "arch": {
              "0": {
                "arch": {},
                "data": {}
              },
              "1": {
                "arch": {},
                "data": {}
              },
              "2": {
                "arch": {},
                "data": {}
              },
              "3": {
                "arch": {},
                "data": {}
              },
              "4": {
                "arch": {},
                "data": {}
              },
              "5": {
                "arch": {},
                "data": {}
              },
              "6": {
                "arch": {},
                "data": {}
              },
              "7": {
                "arch": {},
                "data": {}
              },
              "8": {
                "arch": {},
                "data": {}
              },
              "9": {
                "arch": {},
                "data": {}
              },
              "10": {
                "arch": {},
                "data": {}
              },
              "11": {
                "arch": {},
                "data": {}
              },
              "12": {
                "arch": {},
                "data": {}
              },
              "13": {
                "arch": {},
                "data": {}
              },
              "14": {
                "arch": {},
                "data": {}
              },
              "15": {
                "arch": {},
                "data": {}
              },
              "16": {
                "arch": {},
                "data": {}
              },
              "17": {
                "arch": {},
                "data": {}
              },
              "18": {
                "arch": {},
                "data": {}
              },
              "19": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Intel(R)Xeon(R)CPUE5-2698v4@2.20GHz",
              "core": 20,
              "joule(J)": {
                "etotal(J)": 16955,
                "ecpu(J)": 14534,
                "edram(J)": 2421,
                "etotal(%)": 16.4
              }
            }
          },
          "1": {
            "arch": {
              "20": {
                "arch": {},
                "data": {}
              },
              "21": {
                "arch": {},
                "data": {}
              },
              "22": {
                "arch": {},
                "data": {}
              },
              "23": {
                "arch": {},
                "data": {}
              },
              "24": {
                "arch": {},
                "data": {}
              },
              "25": {
                "arch": {},
                "data": {}
              },
              "26": {
                "arch": {},
                "data": {}
              },
              "27": {
                "arch": {},
                "data": {}
              },
              "28": {
                "arch": {},
                "data": {}
              },
              "29": {
                "arch": {},
                "data": {}
              },
              "30": {
                "arch": {},
                "data": {}
              },
              "31": {
                "arch": {},
                "data": {}
              },
              "32": {
                "arch": {},
                "data": {}
              },
              "33": {
                "arch": {},
                "data": {}
              },
              "34": {
                "arch": {},
                "data": {}
              },
              "35": {
                "arch": {},
                "data": {}
              },
              "36": {
                "arch": {},
                "data": {}
              },
              "37": {
                "arch": {},
                "data": {}
              },
              "38": {
                "arch": {},
                "data": {}
              },
              "39": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Intel(R)Xeon(R)CPUE5-2698v4@2.20GHz",
              "core": 20,
              "joule(J)": {
                "etotal(J)": 16343,
                "ecpu(J)": 13802,
                "edram(J)": 2541,
                "etotal(%)": 15.81
              }
            }
          },
          "gpu-nvidia-0": {
            "arch": {
              "1000": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8659,
                "ecpu(J)": 8659,
                "edram(J)": 0,
                "etotal(%)": 8.38
              }
            }
          },
          "gpu-nvidia-1": {
            "arch": {
              "1001": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8847,
                "ecpu(J)": 8847,
                "edram(J)": 0,
                "etotal(%)": 8.56
              }
            }
          },
          "gpu-nvidia-2": {
            "arch": {
              "1002": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8660,
                "ecpu(J)": 8660,
                "edram(J)": 0,
                "etotal(%)": 8.38
              }
            }
          },
          "gpu-nvidia-3": {
            "arch": {
              "1003": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8753,
                "ecpu(J)": 8753,
                "edram(J)": 0,
                "etotal(%)": 8.47
              }
            }
          },
          "gpu-nvidia-4": {
            "arch": {
              "1004": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8712,
                "ecpu(J)": 8712,
                "edram(J)": 0,
                "etotal(%)": 8.43
              }
            }
          },
          "gpu-nvidia-5": {
            "arch": {
              "1005": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8844,
                "ecpu(J)": 8844,
                "edram(J)": 0,
                "etotal(%)": 8.55
              }
            }
          },
          "gpu-nvidia-6": {
            "arch": {
              "1006": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8943,
                "ecpu(J)": 8943,
                "edram(J)": 0,
                "etotal(%)": 8.65
              }
            }
          },
          "gpu-nvidia-7": {
            "arch": {
              "1007": {
                "arch": {},
                "data": {}
              }
            },
            "data": {
              "modelname": "Tesla V100-SXM2-32GB",
              "core": 1,
              "joule(J)": {
                "etotal(J)": 8664,
                "ecpu(J)": 8664,
                "edram(J)": 0,
                "etotal(%)": 8.38
              }
            }
          }
        }
      }
    }
  }
}